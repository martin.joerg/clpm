(in-package #:clpm-test)

(para:define-test quicklisp-bundle
  (uiop:with-current-directory ((asdf:system-relative-pathname :clpm "test/quicklisp-bundle/"))
    (with-clpm-env ()
      (clpm '("bundle" "install" "-y" "--no-resolve")
            :output :interactive
            :error-output :interactive)
      (clpm '("bundle" "exec"
              "--"
              "sbcl" "--non-interactive" "--no-userinit" "--no-sysinit"
              "--eval" "(require :asdf)"
              "--eval" "(asdf:load-system :quicklisp-bundle)"
              "--quit")
            :output :interactive
            :error-output :interactive))))
