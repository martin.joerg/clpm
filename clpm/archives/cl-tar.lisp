;;;; Support for extracting archives using the cl-tar project.
;;;;
;;;; This software is part of CLPM. See README.org for more information. See
;;;; LICENSE for license information.

(uiop:define-package #:clpm/archives/cl-tar
    (:use #:cl
          #:clpm/archives/defs)
  (:import-from #:tar)
  (:import-from #:tar-extract))

(in-package #:clpm/archives/cl-tar)

(defclass cl-tar-client ()
  ()
  (:documentation
   "Describes an archive extractor that uses the cl-tar project."))

(register-tar-client :cl-tar 'cl-tar-client)

(defmethod tar-client-available-p ((client cl-tar-client))
  "Always available."
  t)

(defmethod unarchive-tar ((client cl-tar-client) archive-stream destination-pathname
                          &key strip-components)
  "Ensures ~destination-pathname~ exists and then uses CL-TAR to extract the
contents. Dereferences links on Windows and skips all block and character
devices."
  (assert (uiop:directory-pathname-p destination-pathname))
  (ensure-directories-exist destination-pathname)
  (tar:with-open-archive (a archive-stream)
    (tar-extract:extract-archive a
                                 :directory destination-pathname
                                 :strip-components strip-components
                                 :if-exists :replace
                                 :if-newer-exists :replace
                                 :no-same-owner t
                                 :symbolic-links (if (uiop:os-windows-p)
                                                     :dereference
                                                     t)
                                 :hard-links (if (uiop:os-windows-p)
                                                 :dereference
                                                 t)
                                 :fifos (if (uiop:os-windows-p)
                                            :skip
                                            t)
                                 :character-devices :skip
                                 :block-devices :skip)))
